# minisys-os-doc

该项目是在MIPS FPGA SoC基础之上的操作系统项目文档.

## 目录

- 准备工作
  - [硬件](0_preparation/1_hardware.md)
  - [开发环境](0_preparation/2_dev_env.md)
  - [GDB使用](0_preparation/3_gdb.md)
  - [makefile与lds](0_preparation/4_makefile_lds.md)
  - [bootloader](0_preparation/5_bootloader.md)
- 驱动
  - [LED等简单的外设](1_drivers/1_leds_etc.md)
  - [定时器](1_drivers/2_timer.md)
  - [串口](1_drivers/3_uart.md)
- 内存管理
  - [内存规划](2_mm/1_planning.md)
  - [物理内存管理](2_mm/2_pm_management.md)
  - [虚拟内存管理](2_mm/3_mv_management.md)
- 进程与异常
- 文件系统

## 文档规范

使用vscode和[Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)插件编写.

1. 图片文件集中在同级目录的`img`文件夹下. 其他文件集中在同级目录的`assets`文件夹下.
2. 文件命名以序号和文章名称, 图片和其他文件命名以文章序号和子序号, 用下划线连接.
3. 一般使用英文标点, 英文标点`,.`后带一个空格. 中英文连接的空格自行斟酌.
4. 叶子节点的文档用插件生成目录, 格式按已有文档作为模板.
5. 未完成的文档请在下方添加todo list, 若完成则**删除**对应条目.

## 待完成

- [ ] 内存规划
- [ ] 物理内存管理
- [ ] 虚拟内存管理