# LED等简单的外设

- [LED等简单的外设](#led%E7%AD%89%E7%AE%80%E5%8D%95%E7%9A%84%E5%A4%96%E8%AE%BE)
  - [硬件地址](#%E7%A1%AC%E4%BB%B6%E5%9C%B0%E5%9D%80)
  - [控制外设](#%E6%8E%A7%E5%88%B6%E5%A4%96%E8%AE%BE)

## 硬件地址

外设的所有地址都在`inc/mfp.io.h`中定义, 如下所示:

```h
#define LEDS_ADDR           0xB0600000
#define SWITCHES_ADDR       0xB0C00000
#define SEVEN_SEG_EN_ADDR   0xB0800000
#define SEVEN_SEG_ADDR      0xB0810000
#define UART_ADDR           0xB0400000
#define TIMER_ADDR          0xB0500000
```

## 控制外设

本次介绍简单的外设, 有LED灯、拨码开关、七段数码管. 设备的驱动都在`drivers`文件夹下. 对于简单外设, 我们通过访问映射到内存地址的寄存器来操作. `mips/cpu.h`提供了两个读写指定地址的接口, 分别是:

```h
#define mips_get_word(addr, errp)	(*(volatile unsigned int *)(addr))
#define mips_put_word(addr, v)	(*(volatile unsigned int *)(addr)=(v), 0)
```

对于LED灯来说, 写值对应设置灯的亮暗. 对于拨码开关来说, 读值对应开关状态.

七段数码管由两个地址控制, 其中`SEVEN_SEG_EN_ADDR`为控制数码管亮灭, 1为有效; `SEVEN_SEG_ADDR`控制数码管的值.
