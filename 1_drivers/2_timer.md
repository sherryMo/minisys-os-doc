# 定时器

- [定时器](#%E5%AE%9A%E6%97%B6%E5%99%A8)
  - [硬件文档](#%E7%A1%AC%E4%BB%B6%E6%96%87%E6%A1%A3)
  - [控制外设](#%E6%8E%A7%E5%88%B6%E5%A4%96%E8%AE%BE)

## 硬件文档

我们使用的是AXI Timer v2.0.

具体参阅 https://www.xilinx.com/support/documentation/ip_documentation/axi_timer/v2_0/pg079-axi-timer.pdf

## 控制外设

定时器基地址从`0xB0500000`开始, 根据文档来完成所需的寄存器的读写.

我们只使用定时器的timer0, 提供开启关闭定时器、开启关闭中断和设置定时的三个主要功能. 与我们功能相关联的寄存器分别是Control/Status Register 0和Load Register 0.

其中Control/Status Register 0通过设置第7位启用计时, 第6位启用中断, 第4位当计数溢出时自动从Load Register 0载入, 第1位设置向下计数. 通过设置Load Register 0的值来控制中断的频率.

我们的平台时钟为50MHz, 一秒定时根据文档公式为`1秒 * 50MHz - 4`.
